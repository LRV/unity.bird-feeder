using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

namespace Scripts.Main
{
    public class TransactionCommand : EventCommand
	{
        /*
        private bool _hasDelegates;
        private string _lastID;

        [Inject]
        public IAnalytics Analytics { get; set; }

        public override void Execute()
        {
            PurchasableItem purchasableItem = evt.data as PurchasableItem;
            //
            if (!_hasDelegates) 
            {
                _hasDelegates = true;
                // We must first hook up listeners to Unibill's events.
                Unibiller.onBillerReady += onBillerReady;
                Unibiller.onTransactionsRestored += onTransactionsRestored;
                Unibiller.onPurchaseCancelled += onCancelled;
                Unibiller.onPurchaseFailed += onFailed;
                Unibiller.onPurchaseCompleteEvent += onPurchased;
            }
            Unibiller.initiatePurchase(purchasableItem);
            
            _lastID = purchasableItem.Id;
            Analytics.InAppPurchase(_lastID, AnalyticsModel.MenuAction.Presented);

        }

        /// <summary>
        /// This will be called when Unibill has finished initialising.
        /// </summary>
        private void onBillerReady(UnibillState state)
        {
            Debug.Log("onBillerReady:" + state);
        }

        /// <summary>
        /// This will be called after a call to Unibiller.restoreTransactions().
        /// </summary>
        private void onTransactionsRestored(bool success)
        {
            Debug.Log("Transactions restored.");
        }

        /// <summary>
        /// This will be called when a purchase completes.
        /// </summary>
        private void onPurchased(PurchaseEvent e)
        {
            Debug.Log("Purchase OK: " + e.PurchasedItem.Id);
            Debug.Log("Receipt: " + e.Receipt);
            Debug.Log(string.Format("{0} has now been purchased {1} times.",
                e.PurchasedItem.name,
                Unibiller.GetPurchaseCount(e.PurchasedItem)));
            //
            dispatcher.Dispatch(MainEvent.TRANSACTION_COMPLETE);

            Analytics.InAppPurchase(_lastID, AnalyticsModel.MenuAction.Bought);
        }

        /// <summary>
        /// This will be called if a user opts to cancel a purchase
        /// after going to the billing system's purchase menu.
        /// </summary>
        private void onCancelled(PurchasableItem item)
        {
            Debug.Log("Purchase cancelled: " + item.Id);
            Analytics.InAppPurchase(_lastID, AnalyticsModel.MenuAction.Canceled);
        }

        /// <summary>
        /// This will be called is an attempted purchase fails.
        /// </summary>
        private void onFailed(PurchasableItem item)
        {
            Debug.Log("Purchase failed: " + item.Id);
        }
         * */
	}
}

