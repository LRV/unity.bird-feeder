using System;
using strange.extensions.command.impl;

namespace Scripts.Main
{
	public class LoadSceneCommand : EventCommand
	{
		
		public override void Execute()
		{
			string filepath = evt.data.ToString();
			
			//Load the component
			if (String.IsNullOrEmpty(filepath))
			{
				throw new Exception("Can't load a module with a null or empty filepath.");
			}
            dispatcher.Dispatch(MainEvent.LOAD_STARTED, filepath);
        }
	}
}

