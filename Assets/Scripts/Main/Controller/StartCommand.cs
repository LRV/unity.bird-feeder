using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;

namespace Scripts.Main
{
	public class StartCommand : EventCommand
	{

        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        public override void Execute()
        {

            // Now we're ready to initialise Unibill.
            //Unibiller.Initialise();

            //
            //-----------View Setup--------------
            //
            GameObject slv = new GameObject();
            slv.name = "SceneLoaderView";
            slv.transform.parent = contextView.transform;
            slv.AddComponent<SceneLoaderView>();
            
            //	
            GameObject mlv = new GameObject();
            mlv.name = "MenuLoaderView";
            mlv.transform.parent = contextView.transform;
            mlv.AddComponent<MenuLoaderView>();
            
            //
            //AudioSetUp();
        }

        /*
	    private void AudioSetUp()
	    {
            //Music
            if (PlayerProfile.MusicOn == "")
            {
                PlayerProfile.MusicOn = "true";
            }

            if (PlayerProfile.MusicOn == "false")
            {
                //MasterAudio.PlaylistsMuted = true;
            }

            //All sounds
            if (PlayerProfile.SoundsOn == "")
            {
                PlayerProfile.SoundsOn = "true";
            }

            if (PlayerProfile.SoundsOn == "false")
            {
                //MasterAudio.MixerMuted = true;
            }
	    }
         */
	}
}

