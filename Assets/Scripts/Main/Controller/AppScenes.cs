using System;

namespace Scripts.Main
{
	public enum AppScenes
	{
        StartScreen,
        PickYourPup,
        WorldMap,
        ItemShop,
        MyPup,
        Game,
        TestLevel,
        Comics,
	    Credits
	}
}

