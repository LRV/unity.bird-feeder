using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Scripts.Main
{
    public class SceneLoaderMediator : EventMediator
	{
		[Inject]
        public SceneLoaderView View { get; set; }

        [Inject]
        public IAppInfo AppInfo { get; set; }

        

        //[Inject]
        //public IAnalytics Analytics { get; set; }

        private AsyncOperation _sceneLoader;
        private string _lastAppScene;
		public override void OnRegister()
		{
            //
            DontDestroyOnLoad(contextView);
			//Listen to the global event bus for events
            dispatcher.AddListener(MainEvent.LOAD_STARTED, StartLoading);
            //StartCoroutine(LoadVeiwAssets());
		    View.init ();
            dispatcher.Dispatch(MainEvent.LOAD_SCENE, AppScenes.Game);
		}
		
		public override void OnRemove()
		{
			//Clean up listeners when the view is about to be destroyed
            dispatcher.RemoveListener(MainEvent.LOAD_STARTED, StartLoading);
		}
        void FixedUpdate()
        {
            if (_sceneLoader != null)
            {
                //Debug.Log(_sceneLoader.progress);
                View.UpdateProgressBar(_sceneLoader.progress);
                if (_sceneLoader.progress >= 1) { _sceneLoader = null; }
            }
        }

        private void StartLoading(IEvent evt)
        {
            AppInfo.CurrentScene = (AppScenes)Enum.Parse(typeof(AppScenes), evt.data.ToString());
            StartCoroutine(LoadNewScene(AppInfo.CurrentScene.ToString()));
		}

        IEnumerator LoadNewScene(string sceneName)
        {
            //Analytics.SceneChange(sceneName);

            View.ShowLoader();
            dispatcher.Dispatch(MainEvent.CLEAR_ALL_MENUS);
            yield return new WaitForSeconds(.5f);
            //
            _sceneLoader = Application.LoadLevelAsync(sceneName);
            _lastAppScene = sceneName;

            //delay for animation
            yield return _sceneLoader;
            yield return new WaitForSeconds(.5f);
            //delay for animation
            View.HideLoader();
        }

        IEnumerator LoadVeiwAssets()
        {
            //delay for animation
            AsyncOperation Loader = Application.LoadLevelAdditiveAsync("SceneLoader");
            yield return Loader;
            //
            GameObject go = GameObject.Find("SceneLoaderRoot");
            go.transform.parent = View.transform;
            //
            View.init();
            LoadStartScreen();
        }


        private void LoadStartScreen()
        {
            dispatcher.Dispatch(MainEvent.LOAD_SCENE, AppScenes.StartScreen);
        }

        private bool IsCutScene(string oldScene, int lastWorldLoaded, int lastLevelLoaded)
        {
            bool iSCutScene = false;

            

            return iSCutScene;
        }

	}
}

