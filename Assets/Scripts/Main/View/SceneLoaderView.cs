using strange.extensions.mediation.impl;

namespace Scripts.Main
{
    public class SceneLoaderView : View
	{

        //private GameObject _self;
        //private UIProgressBar _loadProgressBar;
        //private TweenAlpha _loaderScreenAlpha;

		internal void init()
		{
            //_self = this.transform.gameObject;
            //NGUI - Tween
            //_loaderScreenAlpha = _self.transform.Find("SceneLoaderRoot/LoaderScreen").GetComponent<TweenAlpha>();
            //_loaderScreenAlpha.value = 0;
            //NGUI - Progress Bar
            //_loadProgressBar = _self.transform.Find("SceneLoaderRoot/LoaderScreen/LoadBarHolder/LoadProgressBar").GetComponent<UIProgressBar>();
		}
		
		void Update()
		{

		}

        public void UpdateProgressBar(float progress)
        {
            //_loadProgressBar.value = progress;
        }

        public void ShowLoader()
        {
            //_loadProgressBar.value = 0;
            //_loaderScreenAlpha.PlayForward();
        }

        public void HideLoader()
        {
            //_loaderScreenAlpha.PlayReverse();
        }
	}
}

