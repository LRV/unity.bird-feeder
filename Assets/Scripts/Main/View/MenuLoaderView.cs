using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;

namespace Scripts.Main
{
    public class MenuLoaderView : View
	{

        //private GameObject _self;
        private GameObject _overlay;
        public GameObject MenuObject;

		internal void init()
		{
            //_self = this.transform.gameObject;

        }
		
		void Update()
		{

		}

        public void PlaceOverlay()
        {
            if (_overlay == null)
            {
                _overlay = GameObject.Find("Overlay");
            }
            _overlay.SetActive(true);
            var topDepthUi = GameObject.Find("TopDepthUI");
            if (topDepthUi != null)
            {
                _overlay.transform.parent = MenuObject.transform;
                _overlay.transform.localPosition = new Vector2(0, 0);
            }
        }

        public void HideOverlay()
        {
            var loaderRoot = GameObject.Find("SceneLoaderRoot");
            _overlay.transform.parent = loaderRoot.transform;
            _overlay.transform.localPosition = new Vector2(0, 1230);
            _overlay.SetActive(false);
        }

	}
}

