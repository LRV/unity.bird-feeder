/*
 * Copyright 2013 ThirdMotion, Inc.
 *
 *	Licensed under the Apache License, Version 2.0 (the "License");
 *	you may not use this file except in compliance with the License.
 *	You may obtain a copy of the License at
 *
 *		http://www.apache.org/licenses/LICENSE-2.0
 *
 *		Unless required by applicable law or agreed to in writing, software
 *		distributed under the License is distributed on an "AS IS" BASIS,
 *		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *		See the License for the specific language governing permissions and
 *		limitations under the License.
 */

/// Example mediator
/// =====================
/// Make your Mediator as thin as possible. Its function is to mediate
/// between view and app. Don't load it up with behavior that belongs in
/// the View (listening to/controlling interface), Commands (business logic),
/// Models (maintaining state) or Services (reaching out for data).

using System;
using System.Collections;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;


namespace Scripts.Main
{
    public class MenuLoaderMediator : EventMediator
	{
		[Inject]
        public MenuLoaderView View { get; set; }

        //[Inject]
        //public IAnalytics Analytics { get; set; }

        private AsyncOperation _menuLoader;
        private GameObject _primaryMenu,_secondaryMenu;
        private Camera _cameraPuppy;
        private string _currentMenu = "";
		public override void OnRegister()
		{
            DontDestroyOnLoad(contextView);
			//Listen to the global event bus for events
            dispatcher.AddListener(MainEvent.CLEAR_ALL_MENUS, ClearAllMenus);
            dispatcher.AddListener(MainEvent.DISPLAY_MENU, StartLoading);
            dispatcher.AddListener(MainEvent.DESTROY_MENU, DestroyMenu);
		    View.init();
		}
		
		public override void OnRemove()
		{
			//Clean up listeners when the view is about to be destroyed
            dispatcher.RemoveListener(MainEvent.CLEAR_ALL_MENUS, ClearAllMenus);
            dispatcher.RemoveListener(MainEvent.DISPLAY_MENU, StartLoading);
            dispatcher.RemoveListener(MainEvent.DESTROY_MENU, DestroyMenu);
		}

        

        private void StartLoading(IEvent evt)
        {
            var newMenu = evt.data as string;
            if (_currentMenu != newMenu + "Root")
            {
                StartCoroutine(LoadNewMenu(newMenu));
            }
		}

        IEnumerator LoadNewMenu(string menuName)
        {
            //Analytics.MenuOpened(menuName);

            _menuLoader = Application.LoadLevelAdditiveAsync(menuName);
            yield return _menuLoader;
            View.MenuObject = GameObject.Find(menuName + "Root");
            View.MenuObject.transform.parent = contextView.transform;
            //
            if (_primaryMenu != null)
            {
                _secondaryMenu = _primaryMenu;
                _secondaryMenu.SetActive(false);
                _primaryMenu = View.MenuObject;
                _currentMenu = _primaryMenu.name;
            }
            else
            {
                _primaryMenu = View.MenuObject;
                _currentMenu = _primaryMenu.name;
            }
        }

        private void DestroyMenu()
        {
            Destroy(_primaryMenu);
            _currentMenu = "";

            if (_secondaryMenu != null)
            {
                _primaryMenu = _secondaryMenu;
                _currentMenu = _primaryMenu.name;
                _primaryMenu.SetActive(true);
                _secondaryMenu = null;
            }
            else
            {
                dispatcher.Dispatch(MainEvent.TOGGLE_PUPPY);
            }

        }

        private void ClearAllMenus()
        {
            Destroy(_primaryMenu);
            _primaryMenu = null;
            _currentMenu = "";
            if (_secondaryMenu != null)
            {
                Destroy(_secondaryMenu);
                _secondaryMenu = null;
            }
        }

        private void TogglePuppyCamera(bool trigger)
        {
            if (GameObject.Find("CameraPuppy") != null)
            {
                _cameraPuppy = GameObject.Find("CameraPuppy").camera;
                _cameraPuppy.enabled = trigger;
            }
        }
   
	}
}

