﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Scripts.Main
{
    public class HighScoreInfo
    {
        public Hashtable HighScores;

        public HighScoreInfo()
        {
            HighScores = new Hashtable();
        }

        public HighScoreInfo(Hashtable table)
        {
            HighScores = table;
        }
    }

    public class HighScoreNode
    {
        public int Id { get; set; }
        public int WorldNum { get; set; }
        public int LevelNum { get; set; }
        public int TopStar { get; set; }
        public int TopScore { get; set; }

        public int TopCoin { get; set; }
        public bool IsLocked { get; set; }
    }

}