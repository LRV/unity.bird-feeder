﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Scripts.Main
{
    [XmlRoot("LevelInfoNode")]
    public class LevelInfoNode
    {
        public int Id { get; set; }
        public int WorldNum { get; set; }
        public int LevelNum { get; set; }
        public string Name { get; set; }
        public string LevelFile { get; set; }
        public int MapPosX { get; set; }
        public int MapPosY { get; set; }
        public LevelType Type { get; set; }
        public int InkTotal { get; set; }
        public int HighRoller { get; set; }
        public int Ink2Star { get; set; }
        public int Ink3Star { get; set; }
        public bool IsItemMagicInk { get; set; }
        public bool IsItemBomb { get; set; }
        public bool IsItemHint { get; set; }
        public bool IsTroll { get; set; }

        public enum LevelType
        {
            Tutorial,
            CollectAll,
            HighRoller,
            CollectInOrder
        }

    }


    public class LevelCatalog
    {
        [XmlArray("LevelList")]
        [XmlArrayItem("LevelInfoNode")]
        public List<LevelInfoNode> LevelList { get; set; }

        public LevelCatalog()
        {
            LevelList = new List<LevelInfoNode>();
        }
    }

}