﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Remoting.Messaging;
using UnityEngine;


namespace Scripts.Main
{

    [XmlRoot("ItemNode")]
    public class ItemNode
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public string Thumbnail { get; set; }

        public enum EarsType
        {
            Inherit,
            EarsUp,
            EarsDown,
            EarsOff
        }

        public enum ItemType
        {
            Body,
            Hat,
            Shirt,
            Supply
        }

        public EarsType Ears { get; set; }

        public ItemType Type { get; private set; }
    }


    public class ItemsOwned
    {
        [XmlArray("ItemsOwnedList")]
        [XmlArrayItem("ItemNode")]
        public List<ItemOwnedNode> ItemsOwnedList;

        public ItemsOwned()
        {
            ItemsOwnedList = new List<ItemOwnedNode>();
        }

        public static void SaveItemsOwned(ItemsOwned io)
        {
            var xmlSerializer = new XmlSerializer(typeof(ItemsOwned));
            var textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, io);

            PlayerPrefs.SetString("itemsowned", textWriter.ToString());
        }

        public static ItemsOwned GetItemsOwned()
        {
            string playerString = PlayerPrefs.GetString("itemsowned");

            if (playerString.Length == 0)
                return new ItemsOwned();

            var xmlSerializer = new XmlSerializer(typeof(ItemsOwned));
            var stringReader = new StringReader(playerString);
            return (ItemsOwned)xmlSerializer.Deserialize(stringReader);
        }
    }

    public class AppliedItems
    {
        public ItemOwnedNode Body;
        public ItemOwnedNode Hat;
        public ItemOwnedNode Shirt;

        public AppliedItems()
        {
            Body = new ItemOwnedNode();
            Hat = new ItemOwnedNode();
            Shirt = new ItemOwnedNode();
        }

        public static void SaveAppliedItems(AppliedItems ai)
        {
            var xmlSerializer = new XmlSerializer(typeof(AppliedItems));
            var textWriter = new StringWriter();

            xmlSerializer.Serialize(textWriter, ai);

            PlayerPrefs.SetString("applieditems", textWriter.ToString());
        }

        public static AppliedItems GetAppliedItems()
        {
            string playerString = PlayerPrefs.GetString("applieditems");

            if (playerString.Length == 0)
                return new AppliedItems();

            var xmlSerializer = new XmlSerializer(typeof(AppliedItems));
            var stringReader = new StringReader(playerString);
            return (AppliedItems)xmlSerializer.Deserialize(stringReader);
        }
    }

    [XmlRoot("ItemNode")]
    public class ItemOwnedNode
    {
        public string SKU { get; set; }
        public ItemNode.EarsType Ears { get; set; }

    }

    public class ShopInventory
    {
        [XmlArray("ItemList")]
        [XmlArrayItem("ItemNode")]
        public List<ItemNode> ItemList { get; set; }

        public ShopInventory()
        {
            ItemList = new List<ItemNode>();
        }
    }

}