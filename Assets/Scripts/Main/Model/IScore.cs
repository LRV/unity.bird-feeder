using System.Collections.Generic;

namespace Scripts.Main
{
    public interface IScore
    {
        void RegisterScore(HighScoreNode newScore);
        void UnlockNextLevel(int worldNumber, int levelNumber);
        string FormatLevelKey(int worldNumber, int levelNumber);
        HighScoreNode GetScore(int worldNumber, int levelNumber);
        void CreateAllScoreRecords(List<LevelInfoNode> levelList);
        void SaveLastPlayedScore(LevelInfoNode level, int currentInk, int score);
        HighScoreNode GetLastPlayedScore();
    }
}