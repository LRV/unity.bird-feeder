﻿using System;
using System.Collections.Generic;

namespace Scripts.Main
{
    public interface IAnalytics
    {
        /*
        //Audio
        void MusicToggle(bool setting);
        void SoundEffectsToggle(bool setting);

        //In-Game
        void LevelStarted(int world, int level, LevelInfoNode.LevelType type, LevelFile levelFile);
        void InkUpdate(int inkCount);
        void TileUpdate(int tileCount);
        void LevelPass(int stars);
        void LevelFail(bool deathByInk, bool deathByTime);
        void LevelRetry();
        void LevelQuit();
        void LilBoomerUsed();
        void MagicInkUsed();
        void HintUsed();
        void Combo2XHappened();
        void Combo3XHappened();
        void Paused();

        //Map Screen
        void MapPreGameMenuOpened(int world, int level);
        void MapPreGameMenuPlayed(int world, int level);
        void MapPreGameMenuCancelled(int world, int level);

        //Misc
        void HowToPlayMenuOpened(int world, int level);

        //Screens
        void SceneChange(string sceneName);
        void MenuOpened(string menuName);

        //Puppy Cutomization
        void CurrentCoints(int coins);
        void DogGender(bool male);
        void SKUPurched(string sku);


        //Purchase Events
        void HeartRefillMenuAction(AnalyticsModel.MenuAction action);
        void InkRefill(AnalyticsModel.MenuAction action);
        void InAppPurchase(string id, AnalyticsModel.MenuAction action);
         */
    }
}
