﻿using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Scripts.Main
{
    public class LevelInfoModel : ILevelInfo
    {
        private readonly LevelCatalog _levelCatalog;
        private LevelInfoNode CurrentLevelInfoNode { get; set; }
        public int WalkableTileCount { get; set; }
        public int CurrentPathCount { get; set; }

        public LevelInfoModel()
        {
            _levelCatalog = LoadLevelCatalogInternally();
        }

        private LevelCatalog LoadLevelCatalogInternally()
        {
            var xml = (TextAsset) Resources.Load("Levels/IndexCatalog", typeof (TextAsset));
            var stringReader = new StringReader(xml.text);

            var serializer = new XmlSerializer(typeof(LevelCatalog));
            var levelxml = serializer.Deserialize(stringReader) as LevelCatalog;

            return levelxml;
        }

        public List<LevelInfoNode> GetAllLevels() 
        {
            return _levelCatalog.LevelList;
        }

        public LevelInfoNode GetLevelById(int id)
        {
            return _levelCatalog.LevelList.Find(x => x.Id == id);
        }

        public LevelInfoNode GetCurrentLevel()
        {
            if (CurrentLevelInfoNode == null)
            {
                CurrentLevelInfoNode = new LevelInfoNode();
                CurrentLevelInfoNode.IsItemBomb = true;
                CurrentLevelInfoNode.IsItemHint = true;
                CurrentLevelInfoNode.IsItemMagicInk = true;
                CurrentLevelInfoNode.IsTroll = true;
            }
            return CurrentLevelInfoNode;
        }

        public void SetCurrentLevel(LevelInfoNode level)
        {
            CurrentLevelInfoNode = level;
            CurrentPathCount = 0;
            WalkableTileCount = 0;
        }

        public bool IsATutorial()
        {
            var tutorialList = new List<int>();
            if (CurrentLevelInfoNode.WorldNum == 0)
            {
                tutorialList.Add(1);
                tutorialList.Add(2);
                tutorialList.Add(3);
                tutorialList.Add(6);
            }
            else
            {
                tutorialList.Add(1);
                tutorialList.Add(5);
                tutorialList.Add(10);
                tutorialList.Add(15);
                tutorialList.Add(20);
                tutorialList.Add(30);
                tutorialList.Add(35);
                tutorialList.Add(55);
                tutorialList.Add(80);
                tutorialList.Add(105);

            }
            bool isTut = tutorialList.IndexOf(CurrentLevelInfoNode.LevelNum) != -1;
            //
            return isTut;
        }
    }
}