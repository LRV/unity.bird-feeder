using System;

namespace Scripts.Main
{
    public interface IPlayerProfile
	{
        bool TestMode { get; set; }
        string Name { get; set; }
        int Hearts { get; set; }
        string Gender { get; set; }
        string MusicOn { get; set; }
        string SoundsOn { get; set; }
        string LastHeartGivenDate { get; set; }
        bool HeartReplenish();
        void LoseOneHeart();
        string TimeTillNextHeart();
	}
}

