﻿using System;
using UnityEngine;

namespace Scripts.Main
{
    public class AnalyticsModel : IAnalytics
    {
        /*
        #region Options

        public void MusicToggle(bool setting)
        {
            GA.API.Design.NewEvent(setting ? "musicon" : "musicoff");
        }

        public void SoundEffectsToggle(bool setting)
        {
            GA.API.Design.NewEvent(setting ? "soundeffecton" : "soundeffectoff");
        }

        #endregion

        #region In-Game

        private int _combo2Count, _combo3Count;
        private int _currentLevelLevel;
        private int _currentLevelWorld;
        private int _inkCount;
        private bool _levelBeingPlayed;
        private float _levelStartTime;
        private LevelInfoNode.LevelType _levelType;
        private int _tileCount;
        private int _tileCountTotal;

        public void LevelStarted(int world, int level, LevelInfoNode.LevelType type, LevelFile levelFile)
        {
            _levelBeingPlayed = true;
            _levelStartTime = Time.time;
            _currentLevelWorld = world;
            _currentLevelLevel = level;
            _levelType = type;
            _tileCountTotal = levelFile.WalkableTileCount();

            GA.API.Design.NewEvent(string.Format("level{0}{1}", _currentLevelWorld, _currentLevelLevel));
            GA.API.Design.NewEvent(string.Format("level{0}{1}:timestart", _currentLevelWorld, _currentLevelLevel), Time.time);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:pathblockers", _currentLevelWorld, _currentLevelLevel),
                levelFile.PathBlockerConnectors.Count);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:speedtiles", _currentLevelWorld, _currentLevelLevel),
                levelFile.GetTileTypeCount(TileType.Speed));
            GA.API.Design.NewEvent(string.Format("level{0}{1}:jumptiles", _currentLevelWorld, _currentLevelLevel),
                levelFile.GetTileTypeCount(TileType.Jump));
            GA.API.Design.NewEvent(string.Format("level{0}{1}:portaltiles", _currentLevelWorld, _currentLevelLevel),
                levelFile.PortalConnectors.Count);
        }

        public void LevelPass(int stars)
        {
            if (!_levelBeingPlayed)
                throw new Exception("LevelPass called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelpass", _currentLevelWorld, _currentLevelLevel));

            switch (stars)
            {
                case 1:
                    GA.API.Design.NewEvent(string.Format("level{0}{1}:levelstar1", _currentLevelWorld, _currentLevelLevel));
                    break;

                case 2:
                    GA.API.Design.NewEvent(string.Format("level{0}{1}:levelstar2", _currentLevelWorld, _currentLevelLevel));
                    break;

                case 3:
                    GA.API.Design.NewEvent(string.Format("level{0}{1}:levelstar3", _currentLevelWorld, _currentLevelLevel));
                    break;
            }

            FinishAndResetTracking();
        }

        public void LevelFail(bool deathByInk, bool deathByTime)
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("LevelFail called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelfail", _currentLevelWorld, _currentLevelLevel));

            if (deathByInk)
                GA.API.Design.NewEvent(string.Format("level{0}{1}:deathbyink", _currentLevelWorld, _currentLevelLevel));

            else if (deathByTime)
                GA.API.Design.NewEvent(string.Format("level{0}{1}:deathbytime", _currentLevelWorld, _currentLevelLevel));

            else
                switch (_levelType)
                {
                    case LevelInfoNode.LevelType.CollectAll:
                        GA.API.Design.NewEvent(string.Format("level{0}{1}:deathbyobjAllTile", _currentLevelWorld,
                            _currentLevelLevel));
                        break;
                    case LevelInfoNode.LevelType.HighRoller:
                        GA.API.Design.NewEvent(string.Format("level{0}{1}:deathbyobjHighRoller", _currentLevelWorld,
                            _currentLevelLevel));
                        break;
                    case LevelInfoNode.LevelType.CollectInOrder:
                        GA.API.Design.NewEvent(string.Format("level{0}{1}:deathbyobjTileInOrder", _currentLevelWorld,
                            _currentLevelLevel));
                        break;
                }

            FinishAndResetTracking();
        }

        public void LevelRetry()
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("LevelRetry called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelretryes", _currentLevelWorld, _currentLevelLevel));

            FinishAndResetTracking();
        }

        public void LevelQuit()
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("LevelQuit called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelquit", _currentLevelWorld, _currentLevelLevel));

            FinishAndResetTracking();
        }

        public void InkUpdate(int inkCount)
        {
            _inkCount = inkCount;
        }

        public void TileUpdate(int tileCount)
        {
            _tileCount = tileCount;
        }


        public void LilBoomerUsed()
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("LilBoomerUsed called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:lilboomerused", _currentLevelWorld, _currentLevelLevel));
        }

        public void MagicInkUsed()
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("MagicInkUsed called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:magicinkused", _currentLevelWorld, _currentLevelLevel));
        }

        public void HintUsed()
        {
            //if (!_levelBeingPlayed)
            //    throw new Exception("HintUsed called with out Started ever being initalized.");

            GA.API.Design.NewEvent(string.Format("level{0}{1}:hintuse", _currentLevelWorld, _currentLevelLevel));
        }

        public void Combo2XHappened()
        {
            _combo2Count++;
        }

        public void Combo3XHappened()
        {
            _combo3Count++;
        }

        public void Paused()
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelpaused", _currentLevelWorld, _currentLevelLevel));
        }

        private void FinishAndResetTracking()
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:inkused", _currentLevelWorld, _currentLevelLevel), _inkCount);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:timeend", _currentLevelWorld, _currentLevelLevel), Time.time);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:tilesplay", _currentLevelWorld, _currentLevelLevel), _tileCount);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:tilesremain", _currentLevelWorld, _currentLevelLevel),
                _tileCountTotal - _tileCount);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:completeiontime", _currentLevelWorld, _currentLevelLevel),
                Time.time - _levelStartTime);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:combo2x", _currentLevelWorld, _currentLevelLevel), _combo2Count);
            GA.API.Design.NewEvent(string.Format("level{0}{1}:combo3x", _currentLevelWorld, _currentLevelLevel), _combo3Count);

            _levelBeingPlayed = false;
            _combo2Count = 0;
            _combo3Count = 0;
            _inkCount = 0;
            _tileCount = 0;
            _tileCountTotal = 0;
        }

        #endregion

        #region Map Screen

        public void MapPreGameMenuOpened(int world, int level)
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelselect", world, level));
        }

        public void MapPreGameMenuPlayed(int world, int level)
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelplayed", world, level));
        }

        public void MapPreGameMenuCancelled(int world, int level)
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:levelcancel", world, level));
        }

        public void HowToPlayMenuOpened(int world, int level)
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:howtoplaybutton", world, level));
        }

        public void MapPreGameMenuHelp(int world, int level)
        {
            GA.API.Design.NewEvent(string.Format("level{0}{1}:helpbutton", world, level));
        }

        #endregion

        #region Screens

        public void SceneChange(string sceneName)
        {
            switch (sceneName)
            {
                case "PickYourPup":
                    GA.API.Design.NewEvent(string.Format("pickyourpup"));
                    break;

                case "MyPup":
                    GA.API.Design.NewEvent(string.Format("vanitystore"));
                    break;

                case "ItemShop":
                    GA.API.Design.NewEvent(string.Format("shopestore"));
                    break;

                case "WorldMap":
                    GA.API.Design.NewEvent(string.Format("stagemapmenu"));
                    break;

                case "Comics":
                    GA.API.Design.NewEvent(string.Format("storyscreen"));
                    break;
            }
        }

        public void MenuOpened(string menuName)
        {
            switch (menuName)
            {
                case "MenuCoins":
                    GA.API.Design.NewEvent(string.Format("getcoins"));
                    break;

                case "MenuHearts":
                    GA.API.Design.NewEvent(string.Format("gethearts"));
                    break;

                case "MenuPlay":
                    GA.API.Design.NewEvent(string.Format("pregamescreen"));
                    break;

                case "MenuPause":
                    GA.API.Design.NewEvent(string.Format("pausemenu"));
                    break;

                case "MenuEmptyInk":
                    GA.API.Design.NewEvent(string.Format("outofinkmenu"));
                    break;

                case "MenuGameOver":
                    GA.API.Design.NewEvent(string.Format("missionfailmenu"));
                    break;

                case "MenuCompleteLevel":
                    GA.API.Design.NewEvent(string.Format("missioncompletemenu"));
                    break;
            }
        }

        #endregion

        #region Puppy Customization

        public void CurrentCoints(int coins)
        {
            GA.API.Design.NewEvent(string.Format("coins"), coins);
        }

        public void DogGender(bool male)
        {
            GA.API.Design.NewEvent(male ? string.Format("dogmale") : string.Format("dogfemale"));
        }

        public void SKUPurched(string sku)
        {
            GA.API.Design.NewEvent(sku);
        }

        #endregion

        #region Purchase Events

        public enum MenuAction
        {
            Presented,
            Bought,
            Canceled
        }


        public void HeartRefillMenuAction(MenuAction action)
        {
            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent("heartrefillpresent");
                    break;

                case MenuAction.Bought:
                    GA.API.Design.NewEvent("heartrefillbrought");
                    break;

                case MenuAction.Canceled:
                    GA.API.Design.NewEvent("heartrefillcancel");
                    break;
            }
        }

        public void InkRefill(MenuAction action)
        {
            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent("inkrefillpresent");
                    break;
                case MenuAction.Bought:
                    GA.API.Design.NewEvent("inkrefillbought");
                    break;
                case MenuAction.Canceled:
                    GA.API.Design.NewEvent("inkrefillcanceled");
                    break;
            }
        }

        public void InAppPurchase(string id, MenuAction action)
        {
            string anaId = "";
            switch (anaId)
            {
                case "com.zuullabs.talltails.powerups_199":
                    anaId = "bestvaluebundle";
                    break;
                case "com.zuullabs.talltails.coin_1499":
                    anaId = "bestvaluepack";
                    break;
                case "com.zuullabs.talltails.coin_199":
                    anaId = "boosterpack";
                    break;
                case "com.zuullabs.talltails.hearts_099":
                    anaId = "heartrefill";
                    break;
                case "com.zuullabs.talltails.hint_099":
                    anaId = "pgshint";
                    break;
                case "com.zuullabs.talltails.boomer_099":
                    anaId = "pgsboomer";
                    break;
                case "com.zuullabs.talltails.extraink_099":
                    anaId = "inkrefill";
                    break;
                case "com.zuullabs.talltails.coin_299":
                    anaId = "megapack";
                    break;
                case "com.zuullabs.talltails.hints_299":
                    anaId = "popularhint";
                    break;
                case "com.zuullabs.talltails.coin_499":
                    anaId = "popularpack";
                    break;
                case "com.zuullabs.talltails.permaheart_999":
                    return; //No id currently
                case "com.zuullabs.talltails.zen_1999":
                    return;
                case "com.zuullabs.talltails.coin_999":
                    anaId = "ultrapack";
                    break;
                default:
                    return;
            }

            if (anaId == "") return;

            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent(anaId + "present");
                    break;
                case MenuAction.Bought:
                    GA.API.Design.NewEvent(anaId + "bought");
                    break;
                case MenuAction.Canceled:
                    GA.API.Design.NewEvent(anaId + "canceled");
                    break;
            }
        }

        public void MagicInkPreGame(MenuAction action)
        {
            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent("pgsmagicinkpresent");
                    break;
                case MenuAction.Bought:
                    GA.API.Design.NewEvent("pgsmagicinkbought");
                    break;
                case MenuAction.Canceled:
                    GA.API.Design.NewEvent("pgsmagicinkcanceled");
                    break;
            }
        }

        public void HintPreGame(MenuAction action)
        {
            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent("pgshintpresent");
                    break;
                case MenuAction.Bought:
                    GA.API.Design.NewEvent("pgshintbought");
                    break;
                case MenuAction.Canceled:
                    GA.API.Design.NewEvent("pgshintcancel");
                    break;
            }
        }

        public void BoomerPreGame(MenuAction action)
        {
            switch (action)
            {
                case MenuAction.Presented:
                    GA.API.Design.NewEvent("pgsboomerpresent");
                    break;
                case MenuAction.Bought:
                    GA.API.Design.NewEvent("pgsboomerbought");
                    break;
                case MenuAction.Canceled:
                    GA.API.Design.NewEvent("pgsboomercancel");
                    break;
            }
        }

        #endregion
         * */
    }
}