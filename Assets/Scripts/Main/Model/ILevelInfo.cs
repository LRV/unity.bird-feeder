using System;
using System.Collections.Generic;

namespace Scripts.Main
{
    public interface ILevelInfo
	{
        List<LevelInfoNode> GetAllLevels();
        LevelInfoNode GetLevelById(int id);
        LevelInfoNode GetCurrentLevel();
        void SetCurrentLevel(LevelInfoNode level);
        int WalkableTileCount { get; set; }
        int CurrentPathCount { get; set; }
        bool IsATutorial();
	}
}

