using System;
using System.Collections.Generic;

namespace Scripts.Main
{
    public interface IAppInfo
	{
        AppScenes CurrentScene { get; set; }
        bool isRewardNotification { get; set; }
	}
}

