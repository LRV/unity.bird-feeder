using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Scripts.Main
{
    public class PlayerProfileModel : IPlayerProfile
    {
        private const int ReplenishTimer = 15;
        private const int UserMaxHearts = 5;

        public bool TestMode { get; set; }

        public PlayerProfileModel()
        {

        }

        public string Name
        {
            get { return PlayerPrefs.GetString("name"); }
            set { PlayerPrefs.SetString("name", value); }
        }

        public int Hearts
        {
            get { return PlayerPrefs.GetInt("hearts"); }
            set { PlayerPrefs.SetInt("name", value); }
        }

        public string Gender
        {
            get
            {
                if (PlayerPrefs.GetString("gender") == "")
                {
                    PlayerPrefs.SetString("gender", "Female");
                }
                return PlayerPrefs.GetString("gender");
            }
            set { PlayerPrefs.SetString("gender", value); }
        }

        public string MusicOn
        {
            get{return PlayerPrefs.GetString("musicOn");}
            set { PlayerPrefs.SetString("musicOn", value); }
        }
        public string SoundsOn
        {
            get { return PlayerPrefs.GetString("SoundsOn"); }
            set
            {
                PlayerPrefs.SetString("SoundsOn", value);
                PlayerPrefs.SetString("musicOn", value);
            }
        }

        public string LastHeartGivenDate
        {
            get
            {
                if (PlayerPrefs.GetString("lastheartgivendate") == "")
                {
                    PlayerPrefs.GetString("lastheartgivendate", DateTime.Now.ToString());
                }
                return PlayerPrefs.GetString("lastheartgivendate");
            }
            set { PlayerPrefs.SetString("lastheartgivendate", value); }
        }

        public bool HeartReplenish()
        {
            var isHeartGiven = false;
            var playerHearts = Hearts;
            if (playerHearts < UserMaxHearts)
            {
                var currentTime = DateTime.Now;
                if (LastHeartGivenDate == "")
                {
                    //TODO: init 5 start hearts 
                    LastHeartGivenDate = currentTime.ToString();
                }
                else
                {
                    var heartGivenDate = DateTime.Parse(LastHeartGivenDate, CultureInfo.InvariantCulture);
                    //checks if days since last bonus
                    var minutesSinceLastHeart = ((heartGivenDate - currentTime).TotalMinutes)*-1;
                    if (minutesSinceLastHeart >= ReplenishTimer)
                    {
                        var newHeartsTotal = Math.Floor(minutesSinceLastHeart / ReplenishTimer);
                        var leftOverTime = minutesSinceLastHeart % ReplenishTimer;
                        Hearts += Convert.ToInt32(newHeartsTotal);
                        //heart check
                        playerHearts = Hearts;
                        while (playerHearts > UserMaxHearts)
                        {
                            Hearts--;
                            playerHearts = Hearts;
                        }
                        //leftover mins return to timestamp
                        var duration = new TimeSpan(0, Convert.ToInt32(leftOverTime), 0);
                        LastHeartGivenDate = currentTime.Subtract(duration).ToString();
                        //
                        isHeartGiven = true;
                    }
                }
            }
            return isHeartGiven;
        }

        public string TimeTillNextHeart()
        {
            string duration = "00:00";
            DateTime currentTime = DateTime.Now;
            if (LastHeartGivenDate != "")
            {
                if (Hearts < UserMaxHearts)
                {
                    DateTime heartGivenDate = DateTime.Parse(LastHeartGivenDate, CultureInfo.InvariantCulture);
                    heartGivenDate = heartGivenDate.AddMinutes(ReplenishTimer);
                    TimeSpan span = heartGivenDate.Subtract(currentTime);
                    duration = span.Minutes.ToString("D2") + ":" + span.Seconds.ToString("D2");
                }
            }
            return duration;
        }

        public void LoseOneHeart()
        {
            var hearts = Hearts;
            if (hearts == UserMaxHearts)
            {
                LastHeartGivenDate = DateTime.Now.ToString();
            }
            Hearts--;
        }

    }
}