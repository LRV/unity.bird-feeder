﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Scripts.Main
{
    public class HighScoreModel : IScore
    {
        private HighScoreInfo _highScores;
        private HighScoreNode _lastPlayedScore;
        public HighScoreModel()
        {
            _highScores = LoadUserDataProfile();
            //SaveUserDataProfile();
        }

        private HighScoreInfo LoadUserDataProfile()
        {
            var scoreString = PlayerPrefs.GetString("HighScores");
            //var scoreString = "";
            if (scoreString.Length == 0)
                return new HighScoreInfo();

            var xmlSerializer = new XmlSerializer(typeof(List<HighScoreNode>));
            var stringReader = new StringReader(scoreString);

            var lst = xmlSerializer.Deserialize(stringReader);

            return new HighScoreInfo(ListToHash((List<HighScoreNode>)lst));
        }

        private List<HighScoreNode> HashToList(Hashtable hash)
        {
            var lst = new List<HighScoreNode>();

            foreach (DictionaryEntry node in hash)
                lst.Add((HighScoreNode)node.Value);

            return lst;
        }

        private Hashtable ListToHash(List<HighScoreNode> list)
        {
            var ht = new Hashtable();

            foreach (var node in list)
            {
                string levelKey = FormatLevelKey(node.WorldNum, node.LevelNum);
                ht.Add(levelKey, node);
            }

            return ht;
        }

        private void SaveUserDataProfile()
        {
            var xmlSerializer = new XmlSerializer(typeof(List<HighScoreNode>));
            var textWriter = new StringWriter();
            xmlSerializer.Serialize(textWriter, HashToList(_highScores.HighScores));
            PlayerPrefs.SetString("HighScores", textWriter.ToString());
        }

        public void SaveLastPlayedScore(LevelInfoNode level, int currentInk, int score)
        {
            var hsn = new HighScoreNode();

            hsn.Id = level.Id;
            hsn.WorldNum = level.WorldNum;
            hsn.LevelNum = level.LevelNum;

            hsn.TopStar = 1;
            if (currentInk >= level.Ink2Star) hsn.TopStar = 2;
            if (currentInk >= level.Ink3Star) hsn.TopStar = 3;

            hsn.TopScore = score;
            hsn.TopCoin = hsn.TopScore / 100;
            //Unibiller.CreditBalance("coins",hsn.TopCoin);

            hsn.IsLocked = false;
            _lastPlayedScore = hsn;
            //
            RegisterScore(hsn);
        }

        public HighScoreNode GetLastPlayedScore()
        {
            return _lastPlayedScore;
        }

        public void RegisterScore(HighScoreNode newScore)
        {
            string levelKey = FormatLevelKey(newScore.WorldNum, newScore.LevelNum);

            if (_highScores.HighScores.ContainsKey(levelKey))
            {
                var node = (HighScoreNode)_highScores.HighScores[levelKey];

                if (node.TopStar < newScore.TopStar)
                    node.TopStar = newScore.TopStar;

                if (node.TopScore < newScore.TopScore)
                    node.TopScore = newScore.TopScore;

                UnlockNextLevel(newScore.WorldNum, newScore.LevelNum);
                SaveUserDataProfile();
                return;
            }

            //Wasn't found, so add it
            _highScores.HighScores.Add(levelKey, newScore);
            UnlockNextLevel(newScore.WorldNum, newScore.LevelNum);
            //Save the latest
            SaveUserDataProfile();
        }

        public void CreateAllScoreRecords(List<LevelInfoNode> levelList )
        {
            if (_highScores.HighScores.Count == 0)
            {
                foreach (var level in levelList)
                {
                    string levelKey = FormatLevelKey(level.WorldNum, level.LevelNum);

                    var newScore = new HighScoreNode();
                    newScore.Id = level.Id;
                    newScore.WorldNum = level.WorldNum;
                    newScore.LevelNum = level.LevelNum;
                    newScore.TopStar = 0;
                    newScore.TopScore = 0;
                    newScore.TopCoin = 0;
                    newScore.IsLocked = true;

                    _highScores.HighScores.Add(levelKey, newScore);
                }

                //unlocks first tutorial level
                UnlockNextLevel(0, 0);
                SaveUserDataProfile();
            }

        }

        public void UnlockNextLevel(int worldNum, int levelNum)
        {
            //level 25 should unlock nextworld's first level
            if (levelNum == 25) { worldNum += 1; levelNum = 0; }
            //after tutorial unlock 1st
            if (worldNum == 0 && levelNum == 6) { worldNum += 1; levelNum = 0; }
            //
            if (worldNum < 6)
            {
                int nextLevelNum = levelNum + 1;
                string levelKey = FormatLevelKey(worldNum, nextLevelNum);
                var scoreNode = (HighScoreNode) _highScores.HighScores[levelKey];
                scoreNode.IsLocked = false;
            }
        }

        public string FormatLevelKey(int worldNum, int levelNum)
        {
            string levelKey = string.Format("{0}{1}", worldNum.ToString("D2"), levelNum.ToString("D2"));

            return levelKey;
        }

        public HighScoreNode GetScore(int worldNum, int levelNum)
        {
            string levelKey = FormatLevelKey(worldNum, levelNum);
            //
            return (HighScoreNode)_highScores.HighScores[levelKey];
        }

    }
}