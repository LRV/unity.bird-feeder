﻿using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Scripts.Main
{
    public class AppInfoModel : IAppInfo
    {
        public AppScenes CurrentScene { get; set; }
        public bool isRewardNotification { get; set; }

        public AppInfoModel()
        {
           
        }

    }
}