using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace Scripts.Main
{
	public class MainContext : MVCSContext
	{

		public MainContext (MonoBehaviour view) : base(view)
		{
		}

		public MainContext (MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
		{
		}
		
		protected override void mapBindings()
		{
			//Any event that fire across the Context boundary get mapped here.
            crossContextBridge.Bind(MainEvent.LOAD_SCENE);
            crossContextBridge.Bind(MainEvent.DISPLAY_MENU);
            crossContextBridge.Bind(MainEvent.TOGGLE_PUPPY);
            crossContextBridge.Bind(MainEvent.TRANSACTION_STARTED);
            crossContextBridge.Bind(MainEvent.TRANSACTION_COMPLETE);
            crossContextBridge.Bind(MainEvent.TUTORIAL_ACTION);
            /*
            crossContextBridge.Bind(GameEvent.START_GAME);
            crossContextBridge.Bind(GameEvent.PAUSE_GAME);
            crossContextBridge.Bind(GameEvent.UNPAUSE_GAME);
            crossContextBridge.Bind(GameEvent.QUIT_GAME);
            crossContextBridge.Bind(GameEvent.EXIT_GAME);
            crossContextBridge.Bind(GameEvent.RETRY);
            crossContextBridge.Bind(GameEvent.FORCE_FINISHED);
            crossContextBridge.Bind(GameEvent.SKIT_SETUP);
            crossContextBridge.Bind(GameEvent.SKIT_FINISHED);
            crossContextBridge.Bind(GameEvent.REFILL_INK);
            crossContextBridge.Bind(GameEvent.DIALOGUE_ACTION);
            */

			//
            //Commands
            //
            commandBinder.Bind(ContextEvent.START).To<StartCommand>().Once();
			//commandBinder.Bind(MainEvent.LOAD_SCENE).To<LoadSceneCommand>();
			
            //
            //Models
            //
            injectionBinder.Bind<IPlayerProfile>().To<PlayerProfileModel>().ToSingleton().CrossContext();
            injectionBinder.Bind<IAppInfo>().To<AppInfoModel>().ToSingleton().CrossContext();
            //injectionBinder.Bind<ILevelInfo>().To<LevelInfoModel>().ToSingleton().CrossContext();
            //injectionBinder.Bind<IScore>().To<HighScoreModel>().ToSingleton().CrossContext();
		    //injectionBinder.Bind<IAnalytics>().To<AnalyticsModel>().ToSingleton().CrossContext();

            //
            //Views and Mediators
            //
            mediationBinder.Bind<SceneLoaderView>().To<SceneLoaderMediator>();
            mediationBinder.Bind<MenuLoaderView>().To<MenuLoaderMediator>();
            //mediationBinder.Bind<MainView>().To<MainMediator>();
            /*
            //Coins Menu
            mediationBinder.Bind<MenuCoinsView>().To<MenuCoinsMediator>();
            mediationBinder.Bind<CoinsItemButtonView>().To<CoinsItemButtonMediator>();
            //Play Menu
            mediationBinder.Bind<MenuPlayView>().To<MenuPlayMediator>();
            mediationBinder.Bind<GameItemBuyButtonView>().To<GameItemBuyButtonMediator>();
            //Pause Menu
            mediationBinder.Bind<MenuPauseView>().To<MenuPauseMediator>();
            //Options Menu
            mediationBinder.Bind<MenuOptionsView>().To<MenuOptionsMediator>();
            //Complete Level Menu
            mediationBinder.Bind<MenuCompleteLevelView>().To<MenuCompleteLevelMediator>();
            //Game Over Menu
            mediationBinder.Bind<MenuGameOverView>().To<MenuGameOverMediator>();
            //Game Item Buy Menu
            mediationBinder.Bind<MenuGameItemBuyView>().To<MenuGameItemBuyMediator>();
            //Empty Ink Menu
            mediationBinder.Bind<MenuEmptyInkView>().To<MenuEmptyInkMediator>();
            //Hearts Menu
            mediationBinder.Bind<MenuHeartsView>().To<MenuHeartsMediator>();
            //How To Play Menu
            mediationBinder.Bind<MenuHowToPlayView>().To<MenuHowToPlayMediator>();
            //Dialogue Menu
            mediationBinder.Bind<MenuDialogueView>().To<MenuDialogueMediator>();   
            //Tutorials
            mediationBinder.Bind<TutorialView>().To<TutorialMediator>();
            //Reward
            mediationBinder.Bind<MenuRewardView>().To<MenuRewardMediator>();
             * */
		}

	}
}

